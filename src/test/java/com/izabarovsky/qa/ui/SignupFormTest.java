package com.izabarovsky.qa.ui;

import com.izabarovsky.qa.BaseTest;
import com.izabarovsky.qa.pages.EntryPage;
import com.izabarovsky.qa.pages.JoinPage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.izabarovsky.qa.Configuration.getJoinUrl;
import static org.junit.jupiter.api.Assertions.*;

public class SignupFormTest extends BaseTest {

    private static final String FORM_ERROR_MSG = "There were problems creating your account.";
    private static final String INPUT_USERNAME = "Username";
    private static final String INPUT_EMAIL = "Email address";
    private static final String INPUT_PASSWORD = "Password";

    @DisplayName("signupForm")
    @Test
    void signupForm() {
        JoinPage joinPage = new EntryPage().navigate().signup();
        assertEquals(getJoinUrl(), joinPage.getUrl(), "Assert Url");
        assertAll(
                () -> assertTrue(joinPage.isFormErrorVisible(),
                        "ErrorMsg visibility"),
                () -> assertEquals(FORM_ERROR_MSG, joinPage.getFormErrorMsg(),
                        "ErrorMsg text"),
                () -> assertTrue(joinPage.getItem(INPUT_USERNAME).isPopupVisible(),
                        INPUT_USERNAME + " visibility"),
                () -> assertTrue(joinPage.getItem(INPUT_EMAIL).isPopupVisible(),
                        INPUT_EMAIL + " visibility"),
                () -> assertTrue(joinPage.getItem(INPUT_PASSWORD).isPopupVisible(),
                        INPUT_PASSWORD + " visibility")
        );
    }

}
