package com.izabarovsky.qa.ui;

import com.izabarovsky.qa.BaseTest;
import com.izabarovsky.qa.pages.EntryPage;
import com.izabarovsky.qa.pages.SearchResultPage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchTest extends BaseTest {

    @DisplayName("search")
    @Test
    void search() {
        SearchResultPage searchResult = new EntryPage().navigate().search("selenide");
        assertAll(
                () -> assertEquals(1119, searchResult.getTotal(),
                        "Check total"),
                () -> assertEquals(7, searchResult.filter((s) -> s.getLanguage().equals("Java")).size(),
                        "Count java repos"),
                () -> assertEquals(1, searchResult.filter((s) -> s.getLanguage().equals("C#")).size(),
                        "Count C# repos"),
                () -> assertEquals("MIT license", searchResult.repositories().get(0).getLicense(),
                        "Check first license"),
                () -> assertEquals("MIT license", searchResult.repositories().get(1).getLicense(),
                        "Check second license")
        );
    }

}
