package com.izabarovsky.qa.ui;

import com.izabarovsky.qa.BaseTest;
import com.izabarovsky.qa.pages.JoinPage;
import com.izabarovsky.qa.pages.components.FormWebItem;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserNameFieldTest extends BaseTest {

    private static final String FORM_ERROR_MSG = "Username selenide is not available.";
    private static final String INPUT_USERNAME = "Username";

    @DisplayName("userNameField")
    @Test
    void userNameField() {
        FormWebItem nameItem = new JoinPage().navigate()
                .getItem(INPUT_USERNAME)
                .setValue("selenide");
        assertAll(
                () -> assertTrue(nameItem.isLabelFaulty(), "Username label marked as faulty"),
                () -> assertTrue(nameItem.isInputFaulty(), "Username input marked as faulty"),
                () -> assertTrue(nameItem.getTooltipMsg().contains(FORM_ERROR_MSG), "Tooltip text")
        );
    }

}
