package com.izabarovsky.qa.ui;

import com.izabarovsky.qa.BaseTest;
import com.izabarovsky.qa.pages.CodeReviewPage;
import com.izabarovsky.qa.pages.EntryPage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.izabarovsky.qa.Configuration.getCodeReviewUrl;
import static org.junit.jupiter.api.Assertions.*;

public class MenuTest extends BaseTest {

    @DisplayName("menu")
    @Test
    void menu() {
        new EntryPage().navigate()
                .selectMenu("Why GitHub?")
                .item("Code review");
        CodeReviewPage page = new CodeReviewPage();
        assertEquals(getCodeReviewUrl(), page.getUrl(), "Assert Url");
        page.clickSee();
        assertAll(
                () -> assertTrue(page.isHeaderVisible(), "Header visibility"),
                () -> assertEquals("Write better code", page.getHeaderText(), "Header text"),
                () -> assertTrue(page.isIframeVisible(), "I-frame visibility")
        );
    }


}
