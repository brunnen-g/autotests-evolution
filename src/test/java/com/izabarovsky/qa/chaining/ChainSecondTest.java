package com.izabarovsky.qa.chaining;

import com.izabarovsky.qa.extension.TestChainExtension;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Set;

import static com.izabarovsky.qa.extension.TestChainExtension.getTestsOfClassFailed;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(TestChainExtension.class)
public class ChainSecondTest {

    @BeforeEach
    void assumption() {
        Set<String> failedTests = getTestsOfClassFailed(this.getClass());
        Assumptions.assumeTrue(failedTests.size() == 0, String.format("Skip because failed %s", failedTests));
    }

    @Order(1)
    @Test
    void first() {
        assertTrue(true);
    }

    @Order(2)
    @Test
    void second() {

    }

    @Order(3)
    @Test
    void third() {
        assertFalse(false);
    }
}
