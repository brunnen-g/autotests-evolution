package com.izabarovsky.qa;

import com.izabarovsky.qa.extension.AllureLogsExtension;
import com.izabarovsky.qa.extension.AllureScreenshotsExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.BrowserWebDriverContainer;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;
import static com.izabarovsky.qa.Configuration.isDockerBrowserRequired;

@ExtendWith({AllureScreenshotsExtension.class, AllureLogsExtension.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class BaseTest {

    protected static final Logger LOG = LoggerFactory.getLogger("BaseTest");
    private BrowserWebDriverContainer testContainer;

    @BeforeEach
    void before() {
        if (isDockerBrowserRequired()) initDockerBrowser();
    }

    @AfterEach
    void closeBrowser() {
        closeWebDriver();
    }

    private void initDockerBrowser() {
        testContainer = new BrowserWebDriverContainer()
                .withRecordingMode(
                        BrowserWebDriverContainer.VncRecordingMode.SKIP, null
                );
        testContainer.start();
        setWebDriver(testContainer.getWebDriver());
    }
}
