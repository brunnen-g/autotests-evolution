package com.izabarovsky.qa.wait;

import com.codeborne.selenide.Selenide;

import java.util.function.BooleanSupplier;

public class CustomWait {

    private long timeout = 4000;
    private int pollingTime = 100;

    public CustomWait setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public boolean waitFor(BooleanSupplier supplier) {
        long timer = timeout;
        do {
            if (supplier.getAsBoolean()) return true;
            Selenide.sleep(pollingTime);
            timer -= pollingTime;
        }
        while (timer > 0);
        return false;
    }

}
