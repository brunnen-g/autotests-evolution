package com.izabarovsky.qa.parser;

public interface Parser<T> {

    T parse(String src);

}
