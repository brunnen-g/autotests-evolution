package com.izabarovsky.qa.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RepositoriesTotalParser implements Parser<Long> {

    @Override
    public Long parse(String src) {
        Pattern pattern = Pattern.compile("^(\\d+,\\d+)\\srepository results");
        Matcher matcher = pattern.matcher(src);
        if (matcher.matches() && matcher.groupCount() >= 1) {
            return Long.valueOf(matcher.group(1).replaceAll(",", ""));
        } else throw new IllegalArgumentException(String.format("Cannot parse string [%s]", src));
    }

}
