package com.izabarovsky.qa;

// TODO: 4/30/20 Move to some property file
public class Configuration {

    private static final String BASE_URL = "https://github.com/";
    private static final String JOIN_URL = BASE_URL + "join";
    private static final String CODE_REVIEW_URL = BASE_URL + "features/code-review/";
    private static final boolean DOCKER_BROWSER_USE = true;
    private static final int TIMEOUT_MS = 6000;

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getJoinUrl() {
        return JOIN_URL;
    }

    public static String getCodeReviewUrl() {
        return CODE_REVIEW_URL;
    }

    public static boolean isDockerBrowserRequired() {
        return DOCKER_BROWSER_USE;
    }

    public static int getTimeoutMs() {
        return TIMEOUT_MS;
    }

}
