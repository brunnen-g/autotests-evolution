package com.izabarovsky.qa.pages;


import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.izabarovsky.qa.Configuration.getCodeReviewUrl;
import static com.izabarovsky.qa.Configuration.getTimeoutMs;

public class CodeReviewPage extends BasePage implements Pageable<CodeReviewPage> {

    private static final By HEADER = By.xpath("//div[@class='jumbotron-code-review']//h1");
    private static final By BUTTON_SEE = By.xpath("//div[@class='jumbotron-code-review']//button");
    private static final By IFRAME = By.xpath("//div[@class='jumbotron-code-review']//iframe");

    @Step("Click to see")
    public CodeReviewPage clickSee() {
        $(BUTTON_SEE).waitUntil(enabled, getTimeoutMs()).click();
        return this;
    }

    public boolean isHeaderVisible() {
        return logSimpleWrapper("isHeaderVisible", () -> $(HEADER).exists());
    }

    public String getHeaderText() {
        return $(HEADER).getText();
    }

    public boolean isIframeVisible() {
        return $(IFRAME).isDisplayed();
    }

    @Override
    public CodeReviewPage navigate() {
        return open(getCodeReviewUrl(), CodeReviewPage.class);
    }
}
