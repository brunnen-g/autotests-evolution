package com.izabarovsky.qa.pages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

public abstract class BasePage {
    protected static final Logger LOG = LoggerFactory.getLogger(BasePage.class);

    protected <T> T logSimpleWrapper(String msg, Supplier<T> supplier) {
        T val = supplier.get();
        LOG.info(msg, val);
        return val;
    }
}
