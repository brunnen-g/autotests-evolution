package com.izabarovsky.qa.pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.izabarovsky.qa.Configuration.*;

public class EntryPage implements Pageable {

    private static final By LOGIN_BUTTON = By.xpath("//button[@type='submit']");
    private static final By SEARCH_INPUT = By.xpath("//input[contains(@class, 'search-input')]");
    private static final By NAV_MENU_ITEMS_OPENABLE = By.xpath("//nav/ul/li/details");
    private static final By DROP_DOWN_ITEMS = By.xpath(".//a");

    @Step("Sign up")
    public JoinPage signup() {
        $(LOGIN_BUTTON).waitUntil(enabled, getTimeoutMs()).click();
        if (getUrl().equals(getJoinUrl())) {
            return page(JoinPage.class);
        } else throw new AssertionError("Unknown page");
    }

    @Step("Search")
    public SearchResultPage search(String value) {
        $(SEARCH_INPUT).setValue(value)
                .pressEnter();
        return new SearchResultPage();
    }

    @Step("Select menu item [{0}]")
    public MenuItems selectMenu(String menu) {
        ElementsCollection elements = $$(NAV_MENU_ITEMS_OPENABLE)
                .find(text(menu).because("Cannot find required menu item"))
                .hover().waitUntil(attribute("open"), getTimeoutMs())
                .$$(DROP_DOWN_ITEMS);
        return new MenuItems(elements);
    }

    @Override
    public EntryPage navigate() {
        return open(getBaseUrl(), EntryPage.class);
    }

    public class MenuItems {
        private ElementsCollection elements;

        public MenuItems(ElementsCollection elements) {
            this.elements = elements;
        }

        @Step("Select sub menu item [{0}]")
        public void item(String item) {
            elements.find(text(item)).click();
        }

    }
}
