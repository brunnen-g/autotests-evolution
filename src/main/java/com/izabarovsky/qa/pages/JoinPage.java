package com.izabarovsky.qa.pages;

import com.izabarovsky.qa.pages.components.FormWebItem;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.izabarovsky.qa.Configuration.getJoinUrl;
import static com.izabarovsky.qa.Configuration.getTimeoutMs;

public class JoinPage implements Pageable<JoinPage> {

    private static final By SIGN_UP_FORM = By.cssSelector("#signup-form .flash-error");
    private static final By FORM_ITEMS = By.xpath("//dl[contains(@class, 'form-group')]");

    public boolean isFormErrorVisible() {
        return $(SIGN_UP_FORM).waitUntil(visible, getTimeoutMs()).isDisplayed();
    }

    public String getFormErrorMsg() {
        return $(SIGN_UP_FORM).waitUntil(visible, getTimeoutMs()).getText();
    }

    public FormWebItem getItem(String label) {
        return $$(FORM_ITEMS).stream()
                .map(FormWebItem::new)
                .filter(s -> s.getName().equals(label))
                .findFirst()
                .orElseThrow(() -> new AssertionError(String.format("Item with label [%s] not found", label)));
    }

    @Override
    public JoinPage navigate() {
        return open(getJoinUrl(), JoinPage.class);
    }

}
