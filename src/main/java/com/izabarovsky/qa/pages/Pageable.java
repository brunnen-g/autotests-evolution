package com.izabarovsky.qa.pages;

import com.codeborne.selenide.WebDriverRunner;

public interface Pageable<T> {

    default String getUrl() {
        return WebDriverRunner.url();
    }

    T navigate();

}
