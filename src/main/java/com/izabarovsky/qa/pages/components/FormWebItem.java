package com.izabarovsky.qa.pages.components;

import com.codeborne.selenide.SelenideElement;
import com.izabarovsky.qa.wait.CustomWait;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.izabarovsky.qa.Configuration.getTimeoutMs;

public class FormWebItem {

    private static final By ITEM_NAME = By.xpath(".//label");
    private static final By ITEM_INPUT = By.xpath(".//input");
    private static final By ITEM_TOOLTIP = By.xpath(".//dd[@class='error']");
    private final SelenideElement selenideElement;

    public FormWebItem(SelenideElement selenideElement) {
        this.selenideElement = selenideElement;
    }

    public String getName() {
        return selenideElement.$(ITEM_NAME).getText();
    }

    public boolean isPopupVisible() {
        return selenideElement.$(ITEM_TOOLTIP).isDisplayed();
    }

    public FormWebItem setValue(String value) {
        selenideElement.$(ITEM_INPUT).setValue(value);
        return this;
    }

    public boolean isLabelFaulty() {
        return new CustomWait().setTimeout(getTimeoutMs())
                .waitFor(() -> selenideElement.$(ITEM_TOOLTIP).exists());
    }

    public boolean isInputFaulty() {
        return selenideElement.$(ITEM_INPUT).waitUntil(visible, getTimeoutMs())
                .getAttribute("class").contains("is-autocheck-errored");
    }

    public String getTooltipMsg() {
        return selenideElement.$(ITEM_TOOLTIP).getText();
    }

}
