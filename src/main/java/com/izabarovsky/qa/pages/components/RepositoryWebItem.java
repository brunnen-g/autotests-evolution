package com.izabarovsky.qa.pages.components;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class RepositoryWebItem {

    private static final By PROGRAMMING_LANGUAGE = By.xpath(".//span[@itemprop='programmingLanguage']");
    private static final By LICENSE = By.xpath(".//*[contains(text(), 'license')]");
    private final SelenideElement selenideElement;

    public RepositoryWebItem(SelenideElement selenideElement) {
        this.selenideElement = selenideElement;
    }

    public String getLanguage() {
        return selenideElement.$(PROGRAMMING_LANGUAGE).getText();
    }

    public String getLicense() {
        return selenideElement.$(LICENSE).exists() ? selenideElement.$(LICENSE).getText() : "";
    }

}
