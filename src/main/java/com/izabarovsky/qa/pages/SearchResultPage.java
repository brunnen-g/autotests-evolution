package com.izabarovsky.qa.pages;

import com.izabarovsky.qa.pages.components.RepositoryWebItem;
import com.izabarovsky.qa.parser.RepositoriesTotalParser;
import org.openqa.selenium.By;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchResultPage {

    private static final By CODE_SEARCH_RESULTS = By.xpath("//div[contains(@class, 'codesearch-results')]");
    private static final By RESULTS_HEADER = By.xpath(".//h3");
    private static final By LIST_ITEMS = By.cssSelector(".repo-list li");

    public long getTotal() {
        String total = $(CODE_SEARCH_RESULTS).$(RESULTS_HEADER).getText().trim();
        return new RepositoriesTotalParser().parse(total);
    }

    public List<RepositoryWebItem> repositories() {
        return $$(LIST_ITEMS).stream()
                .map(RepositoryWebItem::new)
                .collect(Collectors.toList());
    }

    public List<RepositoryWebItem> filter(Predicate<RepositoryWebItem> predicate) {
        return repositories().stream().filter(predicate).collect(Collectors.toList());
    }

}
