package com.izabarovsky.qa.extension;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class TestChainExtension implements TestExecutionExceptionHandler {
    private static final Set<KeyValuePair<Class<?>, String>> failedTests = new HashSet<>();

    @Override
    public void handleTestExecutionException(ExtensionContext extensionContext, Throwable throwable) throws Throwable {
        Method method = extensionContext.getTestMethod().orElse(null);
        if (!Objects.isNull(method)) {
            failedTests.add(new KeyValuePair<>(method.getDeclaringClass(), method.getName()));
        }
        throw throwable;
    }

    public static Set<String> getTestsOfClassFailed(Class<?> clazz) {
        return failedTests.stream().filter(s -> s.getKey().equals(clazz))
                .map(KeyValuePair::getValue)
                .collect(Collectors.toSet());
    }

    public static Set<KeyValuePair<Class<?>, String>> getFailedTests() {
        return failedTests;
    }

}
