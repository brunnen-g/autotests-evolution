package com.izabarovsky.qa.extension;

import ch.qos.logback.classic.sift.SiftingAppender;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Collection;

public class TestLogger {

    public static final String TEST_NAME = "testname";

    /**
     * Adds the test name to MDC so that sift appender can use it and log the new
     * log events to a different file
     *
     * @param name name of the new log file
     */
    public static void startTestLogging(String name) {
        MDC.put(TEST_NAME, name);
    }

    /**
     * Removes the key (log file name) from MDC
     *
     * @return name of the log file, if one existed in MDC
     */
    public static String stopTestLogging() {
        String name = MDC.get(TEST_NAME);
        MDC.remove(TEST_NAME);
        return name;
    }

    /**
     * Get log file from SiftAppender
     *
     * @param subString - log file mast contain some string
     * @return
     */
    public static String getLoggerFile(String subString) {
        String resultFileName = null;
        Logger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        if (logger instanceof ch.qos.logback.classic.Logger) {
            ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) logger;
            SiftingAppender siftingAppender = (SiftingAppender) logbackLogger.getAppender("RootSiftAppender");
            Collection<Appender<ILoggingEvent>> listNestedAppenders = siftingAppender
                    .getAppenderTracker()
                    .allComponents();
            resultFileName = listNestedAppenders.stream()
                    .map(s -> (RollingFileAppender) s)
                    .filter(s -> s.getFile().contains(subString))
                    .findFirst()
                    .map(RollingFileAppender::getFile)
                    .orElse("");
        }
        return resultFileName;
    }

}

