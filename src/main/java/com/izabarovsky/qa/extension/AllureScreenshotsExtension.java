package com.izabarovsky.qa.extension;

import com.codeborne.selenide.Screenshots;
import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class AllureScreenshotsExtension implements TestExecutionExceptionHandler {

    @Override
    public void handleTestExecutionException(ExtensionContext extensionContext, Throwable throwable) throws Throwable {
        Path path = Screenshots.takeScreenShotAsFile().toPath();
        extensionContext.getStore(ExtensionContext.Namespace.GLOBAL).put(extensionContext.getUniqueId(), path);
        this.createAttachment(path);
        throw throwable;
    }

    @Attachment("{0}")
    private byte[] createAttachment(Path path) {
        byte[] result;
        if (path == null) {
            result = "Cannot attach file".getBytes();
        } else {
            try {
                result = Files.readAllBytes(path);
            } catch (IOException e) {
                result = ("Some problem when attach file" + e.getMessage()).getBytes();
            }
        }
        return result;
    }

}