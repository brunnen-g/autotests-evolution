package com.izabarovsky.qa.extension;

import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static com.izabarovsky.qa.extension.TestLogger.getLoggerFile;
import static com.izabarovsky.qa.extension.TestLogger.stopTestLogging;
import static java.lang.System.getProperty;

public class AllureLogsExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

    private static boolean allureAttachmentEnabled = Boolean.parseBoolean(
            getProperty("test.listener.extension.allure.attach.log.enable")
    );

    public static boolean isAllureAttachmentEnabled() {
        return allureAttachmentEnabled;
    }

    public static void setAllureAttachmentEnabled(boolean allureAttachmentEnabled) {
        AllureLogsExtension.allureAttachmentEnabled = allureAttachmentEnabled;
    }

    /**
     * Create new log file before test
     *
     * @param extensionContext
     */
    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) {
        String testLogName = extensionContext.getTestClass()
                .map(Class::getSimpleName)
                .orElse("UnknownTestClass")
                .concat('/' + extensionContext.getDisplayName());
        TestLogger.startTestLogging(testLogName);
    }

    /**
     * Add logs into log file, close it, create allure attachment
     *
     * @param extensionContext
     */
    @Override
    public void afterTestExecution(ExtensionContext extensionContext) {
        String logName = stopTestLogging();
        String logFile = getLoggerFile(logName);
        if (allureAttachmentEnabled && !logFile.isEmpty()) {
            createAttachment(logFile);
        }
    }

    /**
     * Attach file to allure report
     *
     * @param filePath
     * @return
     */
    @Attachment(value = "{0}")
    private byte[] createAttachment(String filePath) {
        byte[] result;
        if (filePath == null || filePath.isEmpty()) {
            result = "Cannot find log".getBytes();
        } else {
            try {
                result = Files.readAllBytes(new File(filePath).toPath());
            } catch (IOException e) {
                result = e.getMessage().getBytes();
            }
        }
        return result;
    }
}

