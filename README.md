# Selenide learning

## Run tests
mvn clean test

## Generate allure-report and start jetty
mvn allure:serve

## Execution time
- 4 threads - 24 seconds
- 1 thread  - 59 seconds

## Tasks

### Test case 1

Steps:
1. Navigate to https://github.com/
2. Leave all fields empty at sign up block
3. Click ‘Sign up for Github’

Assertions:
1. User was navigated to https://github.com/join
2. ‘There were problems creating your account.’  error message is visible
3. Error tooltips are visible for all required fields

### Test case 2

Steps:
1. Navigate to https://github.com/
2. Type ‘selenide’ into ‘Username’ field at sign up block

Assertions:
1. Username form is marked as faulty (think carefully how to implement these checks)
2. Username input field has tooltip with ‘Username selenide is not available.’ error message

### Test case 3

Steps:
1. Navigate to https://github.com/
2. Type ‘selenide’ into search input field
3. Press enter

Assertions:
1. 844 repositories in total
2. 7 repositories with Java language
3. 1 with C#
4. First two repositories are licensed under MIT license


### Test case 4

Steps:
1. Navigate to https://github.com/
2. Mouseover at ‘Why GitHub?’ 
3. Click on ‘Code Review’ menu element
4. Click on ‘See what’s possible’ button

Assertions:
1. User was navigated to https://github.com/features/code-review/
2. User sees ‘Write better code’ h1 header
3. Embedded youtube video block is visible
